import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import abc.ConnectionMySql;
import abc.Student;
import static java.nio.file.StandardCopyOption.*;
public class CsvScanner {
	public static void main(String[] args) throws IOException, SQLException {
		String file = new String("C:\\Users\\AYUSHI\\Documents\\Employee.csv");
		Properties p = new Properties();
		String folderPath = p.getProperty("folderPath");
		String ArchiveFolderPath = p.getProperty("ArchiveFolderPath");
		try {
			InputStream input = new FileInputStream("C:\\Users\\AYUSHI\\git\\repository\\abc\\src\\config\\config.properties");
			p.load(input);
			
			System.out.println(p.getProperty("header"));
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		String line = null;
	
		FileReader fr=new FileReader(file);
		BufferedReader br=new BufferedReader(fr);
		
		ArrayList<Employees> empList = new ArrayList();
		List<String> header=Arrays.asList(br.readLine().split(","));
		
		//////////////////////////////HEADER VALIDATION CODE////////////////////////////////
						 
			List<String> header1 = Arrays.asList(p.getProperty("header").split("\\s*,\\s*")); 	 //config file
			boolean FLAG=true;
			for (int i = 0;i<header.size();i++) 
			{
			if(!header.get(i).equals(header1.get(i))) {
			FLAG=false;
			break;
			}
			}
			
				
	/////////////////////////////////////////////////////////////////////////////////////////////

		
		while((line=br.readLine())!=null)
		{
			Employees emp=new Employees();
			String[] obj=line.split(",");
			emp.setFirstname(obj[0]);
			emp.setLastname(obj[1]);
			emp.setLocation(obj[2]);
			emp.setDate(obj[3]);
			
			empList.add(emp);
			
		}
		if(FLAG) {
			boolean inserted=addEmployeeToDB(empList);
			if(inserted) {
				
				System.out.println("..........Files in given folder..........");
				File folder = new File(p.getProperty("folderPath"));
				CsvScanner listFiles = new CsvScanner();
				listFiles.listAllFiles(folder);
				
					System.out.println("enter file path:");
					Scanner a = new Scanner(System.in);
					String data = a.nextLine();
					//Archiving the file
					/*String zipFileName = data.concat(".zip");
					FileOutputStream fos = new FileOutputStream(zipFileName);
					ZipOutputStream zos = new ZipOutputStream(fos);
					zos.putNextEntry(new ZipEntry(data));
					byte[] bytes = Files.readAllBytes(Paths.get(data));
					zos.write(bytes, 0, bytes.length);
					zos.closeEntry();
					zos.close();
					System.out.println("archieved successfully:"+zipFileName);	*/	
					

					File afile =new File(data);

					if(afile.renameTo(new File(p.getProperty("ArchiveFolderPath") + afile.getName()))){
						System.out.println("File is moved successful!");
					}else{
						System.out.println("File is failed to move!");
					}
			}
			

			}
		}
		
	private void listAllFiles(File folder) {
		File[] fileNames = folder.listFiles();
		for (File file : fileNames) {
			if (file.isDirectory()) {
				listAllFiles(file);
			} else {
				try {
					readContent(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void readContent(File file) throws IOException {
		System.out.println("read file " + file.getCanonicalPath());
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String strLine;
		}
	}

	public void readContent(Path filePath) throws IOException {
		System.out.println("read file " + filePath);
		List<String> fileList = Files.readAllLines(filePath);
		System.out.println("" + fileList);
	}


	private static boolean addEmployeeToDB(ArrayList<Employees> empList) throws SQLException {
		PreparedStatement preparedStatement = null;
		Connection connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3307/student1","root","root");
	    
		try{
			System.out.println("header validated");
			
			String sql = "Insert into students(firstname,lastname,location,date) values(?,?,?,?)";
			preparedStatement = connection.prepareStatement(sql);
			for(Employees emp:empList) {
				
					
				    preparedStatement.setString(1, emp.getFirstname());
				    preparedStatement.setString(2, emp.getLastname());
				    preparedStatement.setString(3, emp.getLocation());
				    preparedStatement.setString(4, emp.getDate());
				   
				    preparedStatement.addBatch();
				     System.out.println("Adding employee: "+emp.toString());
								
			}
			preparedStatement.executeBatch();
		
	}
	catch(Exception e) {
		System.out.println("Exception occurs"+e);
		return false;
	}
	finally {
	    if(preparedStatement != null) {
	        preparedStatement.close();
	    }
	}
		return true;
	}
	
}