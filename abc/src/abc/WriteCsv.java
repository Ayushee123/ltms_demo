package abc;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;

import javax.swing.JOptionPane;

public class WriteCsv {

	public static void main(String[] args) {
		String fname = "Roshni";
		String lname = "chawda";
		String location = "pune";
		int date = 20;
		String filepath = "Book1.csv";
		saveRecord(fname,lname,location,date,filepath);
		
	}
	public static void saveRecord(String fname,String lname,String location,int date,String filepath)
	{
		try {
			FileWriter fw = new FileWriter(filepath,true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);
			
			pw.println(fname+","+lname+","+location+","+date+",");
			pw.flush();
			pw.close();
			
			JOptionPane.showMessageDialog(null, "Record saved");
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(null, "Record not saved");
		}
	}

}
