package abc;

public class Student {

		private String firstname;
		private String lastname;
		private String location;
		private String date;
		
		public Student(String firstname, String lastname, String location, String date) {
			
			this.firstname = firstname;
			this.lastname = lastname;
			this.location = location;
			this.date = date;
		}
		public Student() {
			
		}
		public String getFirstname() {
			return firstname;
		}
		public void setFirstname(String firstname) {
			this.firstname = firstname;
		}
		public String getLastname() {
			return lastname;
		}
		public void setLastname(String lastname) {
			this.lastname = lastname;
		}
		public String getLocation() {
			return location;
		}
		public void setLocation(String location) {
			this.location = location;
		}
		public String getDate() {
			return date;
		}
		public void setDate(String date) {
			this.date = date;
		}
		@Override
		public String toString()
		{
			Student student = new Student();
			return String.format(firstname+" "+lastname+" "+location+" "+date+"\n");
		}
	

}
