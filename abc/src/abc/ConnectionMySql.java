package abc;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

public class ConnectionMySql {

	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3307","root","root");
			String query = "SELECT * FROM student1.students";
			Statement stmt = conn.createStatement();
			ResultSet rs= stmt.executeQuery(query);
			while(rs.next()) {
				System.out.println("firstname:"+rs.getString("firstname")+"lastname:"+rs.getString("lastname")+"location:"+rs.getString("location")+"date:"+rs.getString("date"));
			}
			}
			catch(Exception e) {
				System.err.println(e);
			}
		}

	}
