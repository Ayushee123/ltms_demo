package abc;
import java.io.*;
import java.sql.*;
import java.util.ArrayList;
public class DataEntry {

	public static void main(String[] args) {
		
				String jdbcURL = "jdbc:mysql://localhost:3307/student1";
				String username = "root";
				String password = "root";

				String csvFilePath = "C:\\Users\\AYUSHI\\Documents\\Employee.csv";
		
				int batchSize = 20;
		
				Connection connection = null;
		
				try {
					
					connection = DriverManager.getConnection(jdbcURL, username, password);
					connection.setAutoCommit(false);
					
					String sql = "INSERT INTO students (firstname, lastname, location, date) VALUES (?, ?, ?, ?)";
					PreparedStatement statement = connection.prepareStatement(sql);
					
					BufferedReader lineReader = new BufferedReader(new FileReader(csvFilePath));
					String lineText = null;
					
					int count = 0;
					
					lineReader.readLine(); // skip header line
					ArrayList<Student> arrayList = new ArrayList<>();
					while ((lineText = lineReader.readLine()) != null) {
						String[] data = lineText.split(",");
						Student student =new Student(data[0],data[1],data[2],data[3]);
						
						
						
						statement.addBatch();
						
						if (count % batchSize == 0) {
							statement.executeBatch();
							}
}
					lineReader.close();
		
		
					statement.executeBatch();
		
					connection.commit();
					connection.close();
					
				} catch (IOException ex) {
					System.err.println(ex);
				} catch (SQLException ex) {
					ex.printStackTrace();
		
			try {
			connection.rollback();
			} catch (SQLException e) {
				e.printStackTrace();}
				finally {
				}
				}
	}}

