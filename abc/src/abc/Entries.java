package abc;
import java.io.*;
import java.sql.*;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;


public class Entries {

	public static void main(String[] args)  throws Exception {
		
				DriverManager.registerDriver(new com.mysql.jdbc.Driver());
				String mysqlUrl = "jdbc:mysql://localhost:3307/database?useSSL=false";
				Connection con = DriverManager.getConnection(mysqlUrl, "root", "root");
				System.out.println("Connection established......");
				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery("select * from Student1.students");
				ArrayList<Student> arrayList = new ArrayList<>();
				
				while (rs.next()) {
					Student student = new Student();

					student.setFirstname(rs.getString("Firstname"));
					student.setLastname(rs.getString("Lastname"));
					student.setLocation(rs.getString("Location"));
										student.setDate(rs.getString("Date"));
					
					arrayList.add(student);
				}
				System.out.println(arrayList);
				

	}

}
