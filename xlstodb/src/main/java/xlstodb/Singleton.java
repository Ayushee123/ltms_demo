package xlstodb;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;


public class Singleton {
	
	public String username;
	public String password;
	public String jdbcURL;
	public String folderpath;
	public String batchSize;
	
	private final static Singleton INSTANCE = new Singleton();

	private Singleton()  {
		Properties p = new Properties();
	FileInputStream fis = null;
	try {
	fis= new FileInputStream("C:\\Users\\AYUSHI\\git\\xlstodb\\src\\main\\java\\config.properties");
			p.load(fis);
	} catch (FileNotFoundException e1) {
	e1.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	
	this.username= p.getProperty("username");
	this.password= p.getProperty("password");
	this.folderpath= p.getProperty("folderPath");
	this.batchSize=p.getProperty("batchSize");
	this.jdbcURL= p.getProperty("jdbcURL");

	}
	public static Singleton getInstance() {
	return Singleton.INSTANCE;
	}

	public String getUsername() {
	return username;
	}

	public String getPassword() {
	return password;
	}

	public String getJdbcURL() {
	return jdbcURL;
	}

	public String getFolderpath() {
	return folderpath;
	}

	public String getBatchsize() {
	return batchSize;
	}
		}