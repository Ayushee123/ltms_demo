package xlstodb;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.ArrayList;
public class ExcelToDb {

    public static void main(String[] args) throws Exception {
    	
		Singleton s=Singleton.getInstance();
		Properties p = new Properties();
				
		System.out.println("..........Files in given folder..........");
		File folder = new File(s.getFolderpath());
		ExcelToDb listFiles = new ExcelToDb();
		listFiles.listAllFiles(folder);
		
		System.out.println("enter file path:");
		Scanner a = new Scanner(System.in);
		String data = a.nextLine();
		
		int batchSize= Integer.parseInt(s.getBatchsize());
	
		try {
			long start = System.currentTimeMillis();

			FileInputStream inputStream = new FileInputStream(data);

			XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

			XSSFSheet firstSheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = firstSheet.iterator();
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = (Connection) DriverManager.getConnection(s.jdbcURL,s.username,s.password);
			connection.setAutoCommit(false);

			String sql = "INSERT INTO students (firstname, lastname, location, day) VALUES (?, ?, ?, ?)";
			PreparedStatement statement = connection.prepareStatement(sql);
			int count = 0;

			rowIterator.next(); // skip the header row

			while (rowIterator.hasNext()) {
				Row nextRow = rowIterator.next();
				Iterator<Cell> cellIterator = nextRow.cellIterator();
				
				while (cellIterator.hasNext()) {
					Cell nextCell = cellIterator.next();
					int columnIndex = nextCell.getColumnIndex();
			
				switch (columnIndex) {
					case 0:
						String firstname = nextCell.getStringCellValue();
						statement.setString(1, firstname);
					case 1:
						String lastname = nextCell.getStringCellValue();
						statement.setString(2, lastname);
					case 2:
						String location = nextCell.getStringCellValue();
						statement.setString(3, location);
					case 3:
						String day = nextCell.getStringCellValue();
						statement.setString(4, day);
					}

				}

				statement.addBatch();

				if (count % batchSize == 0) {
					statement.executeBatch();
				}
			}
			
			workbook.close();

			// execute the remaining queries
			statement.executeBatch();

			connection.commit();
			connection.close(); 

			long end = System.currentTimeMillis();
			System.out.printf("Import done in %d ms\n", (end - start));
			
		}
		catch(IOException ex1) {
			System.out.println("Error reading file");
			ex1.printStackTrace();
		}
		catch (SQLException ex2) {
			System.out.println("Database error");
			ex2.printStackTrace();
		
		}	
    }

		private void listAllFiles(File folder) {
			File[] fileNames = folder.listFiles();
			for (File file : fileNames) {
				if (file.isDirectory()) {
					listAllFiles(file);
			}   else {
				System.out.println(file);
			}
		}
	}
}